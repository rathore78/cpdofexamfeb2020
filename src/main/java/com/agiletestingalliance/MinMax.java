package com.agiletestingalliance;

public class MinMax {

    public int findMin(int val1, int val2) {
        if (val1 > val2) {
            return val2;
        } else {
            return val1;
        }
    }

    public String getString(String string) {
        if (string != null && !string.equals("")) {
            return string;
        }

        if (string != null && string.equals("")) {
            return string;
        }
        return string;
    }
}

