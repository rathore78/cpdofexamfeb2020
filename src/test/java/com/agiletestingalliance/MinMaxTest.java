package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.*;

public class MinMaxTest {

    @Test
    public void findMin() {
        assertEquals("findmin", 5, new MinMax().findMin(5,8));
    }

    @Test
    public void getString() {
        assertEquals("getStr", "hello", new MinMax().getString("hello"));
    }
}

